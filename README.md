# Simple WebFlux App

## build me

```
$ ./gradlew clean build
```

## start database docker container

```
$ docker-compose up -d
```

## check container

```
$ docker-compose ps

             Name                           Command              State           Ports         
-----------------------------------------------------------------------------------------------
spring-webflux-essentials_db_1   docker-entrypoint.sh postgres   Up      0.0.0.0:5432->5432/tcp

```

Pro-Tip: Use DB Navitor Plugin to access PostgreSQL:
https://plugins.jetbrains.com/plugin/1800-database-navigator

FYI: The r2dbc doesn't support JPA, so you will need to create schemas manually 

## run migrations
```
$ ./gradlew flywayMigrate
```
