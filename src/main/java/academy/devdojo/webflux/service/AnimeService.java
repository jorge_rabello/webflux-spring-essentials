package academy.devdojo.webflux.service;

import academy.devdojo.webflux.model.Anime;
import academy.devdojo.webflux.repository.AnimeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class AnimeService {

    private final AnimeRepository repository;

    public Flux<Anime> findAll() {
        return repository.findAll();
    }

    public Mono<Anime> findById(Long id) {
        return repository.findById(id)
                .switchIfEmpty(monoResponseStatusNotFoundException("Can't find a anime with id: " + id));
    }

    public <T> Mono<T> monoResponseStatusNotFoundException(String message) {
        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, message));
    }
}
