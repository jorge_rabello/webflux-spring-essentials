package academy.devdojo.webflux.exception;

import java.util.Map;
import java.util.Objects;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ResponseStatusException;

@Component
public class CustomAttributes extends DefaultErrorAttributes {

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String, Object> errorAttributesMap = super.getErrorAttributes(request, options);
        Throwable throwable = getError(request);
        if (throwable instanceof ResponseStatusException) {
            ResponseStatusException exception = (ResponseStatusException) throwable;
            String message = Objects.requireNonNull(exception.getMessage()).replace("\"", "");
            errorAttributesMap.put("message", message);
            errorAttributesMap.put("developerMessage", "Response status exception");
        }
        return errorAttributesMap;
    }
}
